#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
import sys
import csv
from operator import itemgetter
from itertools import groupby
from collections import defaultdict

def tran_code(file, outfile, code):

    control = defaultdict(list)

    # with open(file) as fname:
    #     reader = csv.DictReader(fname, delimiter='\t')
    #     info = sorted(reader, key=itemgetter('Gene'))
    #     for k, g in groupby(info, key=itemgetter('Gene')):
    #         for row in g:
    #             acids_source = row['HGVSP'][:3]
    #             acids_lasted = row['HGVSP'][-3:]
    #             acids_pos = row['HGVSP'][3:-3]
    #             if acids_source not in code.keys() or acids_lasted not in code.keys():
    #                 continue
    #             acids_change = code[acids_source]+acids_pos+code[acids_lasted]
    #             control[k].append(acids_change)

    with open(file) as fname:
        reader = csv.DictReader(fname, delimiter='\t')
        info = sorted(reader, key=itemgetter('Gene'))
        for k, g in groupby(info, key=itemgetter('Gene')):
            for row in g:
                if row['Mut'].endswith("*"):
                    row['Mut'] = row['Mut'][:-1]
                control[k].append(row['Mut'])


    with open(outfile, 'w') as wname:
        writer = csv.writer(wname, quoting=csv.QUOTE_MINIMAL, delimiter='\t')
        header = ['Gene', 'HGVSP']
        writer.writerow(header)
        for k, v in control.items():
            vs = ';'.join(v)
            row = [k, vs]
            writer.writerow(row)

if __name__ == '__main__':
    code_dict = {
        'Ala': 'A',
        'Phe': 'F',
        'Cys': 'C',
        'Sec': 'U',
        'Asp': 'D',
        'Asn': 'N',
        'Glu': 'E',
        'Gln': 'Q',
        'Gly': 'G',
        'His': 'H',
        'Leu': 'L',
        'Ile': 'I',
        'Lys': 'K',
        'Pyl': 'O',
        'Met': 'M',
        'Pro': 'P',
        'Arg': 'R',
        'Ser': 'S',
        'Thr': 'T',
        'Val': 'V',
        'Trp': 'W',
        'Tyr': 'Y'
    }

    file = sys.argv[1]
    outfile = sys.argv[2]
    tran_code(file, outfile, code_dict)
