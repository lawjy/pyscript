#!/usr/bin/env python3
# _*_ coding:utf-8 _*_

import sys
import pandas as pd


def add_postion(in_file, in2_file):
    rs_dframe = pd.read_csv(in_file, sep="\t", names=["chrom", "pos", "pos2", "ref", "alt", "rs"])
    panel_dframe = pd.read_csv(in2_file, sep="\t", names=["chrom", "start", "end"])

    rs_collect = []
    for i in range(len(rs_dframe)):
        result = ((str(rs_dframe.loc[i, "chrom"]) == panel_dframe['chrom'].astype("str")) & (rs_dframe.loc[i, "pos"] >= panel_dframe['start']) & (rs_dframe.loc[i, "pos"] <= panel_dframe['end']))
        if result.any():
            rs_collect.append(rs_dframe.loc[i, "rs"])

    rs_dframe = rs_dframe[rs_dframe['rs'].isin(rs_collect)]
    inv_dframe = rs_dframe.loc[:, ["chrom", "pos", "ref", "alt"]]

    return rs_dframe, inv_dframe


if __name__ == '__main__':
    avsnp_file = sys.argv[1]
    panel_file = sys.argv[2]
    rs_file = sys.argv[3]
    intervals_file = sys.argv[4]

    rs_dframe, inv_dframe = add_postion(avsnp_file, panel_file)
    if len(rs_dframe)>0:
        rs_dframe.to_csv(rs_file, sep="\t", index=False)
        inv_dframe.to_csv(intervals_file, sep="\t", index=False, header=False)

