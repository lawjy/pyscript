#!/usr/bin/env python3
# _*_ coding:utf-8 _*_

import sys
import pandas as pd


def read_pass(in_file, in2_file, in3_file):
    var_dframe = pd.read_csv(in_file, sep="\t")
    rs_dframe = pd.read_csv(in2_file, sep="\t")
    mp_dframe = pd.read_csv(in3_file, sep="\t", names=["chrom", "pos", "ref", "depth", "type", "qual"])

    var_dframe.dropna(subset=["avsnp147"])
    return var_dframe, rs_dframe, mp_dframe


def get_Vgenotype(dframe, indices, lower=5, upper=95):
    genotype_list = []
    sub_dframe = dframe[dframe['avsnp147'].isin(indices)]
    out_dframe = sub_dframe.loc[:,["AP7", "avsnp147", "Filter", "REF", "ALT", "PERCENT", "Total_Depth", "Alter_Depth"]]

    for ref, alt, flags, percent in zip(sub_dframe['REF'], sub_dframe['ALT'], sub_dframe['Filter'], sub_dframe['PERCENT']):
        if 'REJECT' in flags:
            type = ref + "/" + ref
            genotype_list.append(type)
        else:
            if percent >= upper:
                type = alt + "/" + alt
                genotype_list.append(type)
            elif percent < lower:
                type = ref + "/" + ref
                genotype_list.append(type)
            else:
                type = ref + "/" + alt
                genotype_list.append(type)

    out_dframe['RsID_Genotype'] = genotype_list
    out_dframe.rename(columns={'avsnp147': 'RSID'}, inplace=True)
    return out_dframe


def get_MPgenotype(rs_dframe, mp_dframe, samples_id, threshold=1000):
    genotype_list = []
    if len(mp_dframe):
        for depth, ref in zip(mp_dframe['depth'], mp_dframe['ref']):
            if depth >= threshold:
                type = ref + "/" + ref
                genotype_list.append(type)
            else:
                genotype_list.append('undetected')
        mp_dframe['RsID_Genotype'] = genotype_list
    else:
        sys.exit()

    rs_collect = []
    for i in range(len(mp_dframe)):
        result = ((str(mp_dframe.loc[i, "chrom"]) == rs_dframe['chrom'].astype("str")) & (mp_dframe.loc[i, "pos"] == rs_dframe['pos']))
        if result.any():
            rs_collect.append(rs_dframe[result].loc[rs_dframe[result].index[0], 'rs'])

    mp_dframe['RSID'] = rs_collect
    mp_dframe['AP7'] = [samples_id]*len(mp_dframe)
    mp_dframe['Filter'] = ['-'] * len(mp_dframe)
    mp_dframe['REF'] = mp_dframe['ref']
    mp_dframe['ALT'] = ['-'] * len(mp_dframe)
    mp_dframe['PERCENT'] = ['-'] * len(mp_dframe)
    mp_dframe['Total_Depth'] = mp_dframe['depth']
    mp_dframe['Alter_Depth'] = ['-'] * len(mp_dframe)
    out_dframe = mp_dframe.loc[:, ["AP7", "RSID", "Filter", "REF", "ALT", "PERCENT", "Total_Depth", "Alter_Depth", "RsID_Genotype"]]
    return out_dframe




if __name__ == '__main__':
    varaint_file = sys.argv[1]
    rspanel_file = sys.argv[2]
    mpileup_file = sys.argv[3]
    out_file = sys.argv[4]


    var_dframe, rs_dframe, mp_dframe = read_pass(varaint_file, rspanel_file, mpileup_file)
    if len(var_dframe):
        var_gdframe = get_Vgenotype(var_dframe, rs_dframe['rs'])
        mp_gdframe = get_MPgenotype(rs_dframe, mp_dframe, var_dframe['AP7'][0])
        out_dframe = pd.concat([var_gdframe, mp_gdframe])
    else:
        out_dframe = get_MPgenotype(rs_dframe, mp_dframe, var_dframe['AP7'][0])
        pass
    out_dframe.to_csv(out_file, sep="\t", index=False)

