#!/usr/bin/env python3
# _*_ coding:utf-8 _*_


import csv
import argparse
import Bio
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqIO.QualityIO import FastqGeneralIterator

"""
ZA : ZA023 GGCTCTAACGTA
ZA_SEQ : GGCTCT + N + AACGTA +T
reserve_ZA_SEQ : A + TACGTT
"""


def build_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--in1', required=True,
                        help="First read sequences along with the Phred-like quality scores are stored in a FASTQ file")
    parser.add_argument('-in2', '--in2', required=True,
                        help="Reverse read sequences along with the Phred-like quality scores are stored in a FASTQ file")
    parser.add_argument('-z', '--za', required=True,
                        help="ZA mark")
    parser.add_argument('-c', '--conf', required=True,
                        help="Sequences with ZA mark are stored in a TXT file")
    parser.add_argument('-o', '--out1', required=False,
                        default='',
                        help='First read sequences after processing are stored in a FASTQ file, default=Re_$in')
    parser.add_argument('-o2', '--out2', required=False,
                        default='',
                        help='Reverse read sequences after processing are stored in a FASTQ file, default=Re_$in2')
    args = parser.parse_args()
    return args


def read_trim(fq1, out1, fq2, out2, primer):
    ##读取reads 文件
    primer1 = primer[0:6] + 'A' + primer[6:] + 'T'
    primer2 = primer[0:6] + 'T' + primer[6:] + 'T'
    primer3 = primer[0:6] + 'C' + primer[6:] + 'T'
    primer4 = primer[0:6] + 'G' + primer[6:] + 'T'
    adaptor = 'A' + Seq(primer).reverse_complement()[0:6]

    len_primer = len(primer1)
    available_title = []

    ###fq1
    out_handle1 = open(out1, 'w')
    with open(fq1, 'r') as in_handle:
        for title, seq, qual in FastqGeneralIterator(in_handle):
            if seq.startswith(primer1) or seq.startswith(primer2) or seq.startswith(
                    primer3) or seq.startswith(primer4):
                available_title.append(title.split( )[0])
                #available_title.append(title.replace("1:N:0", "2:N:0"))
                seq = seq[len_primer:]
                out_handle1.write("@%s\n%s\n+\n%s\n" % (title, seq, qual))
    out_handle1.close()


    orchid_dict = SeqIO.index(fq2, "fastq")
    #orchid_dict = SeqIO.index_db(fq2+'.idx', fq2, "fastq")
    records = [orchid_dict[title] for title in available_title]
    SeqIO.write(records, "tmp_"+fq2, "fastq")


    # fq2
    out_handle2 = open(out2, 'w')
    with open("tmp_"+fq2, 'r') as in_handle2:
        for title, seq, qual in FastqGeneralIterator(in_handle2):
            index = Seq(seq).find(adaptor)
            if index == -1:
                out_handle2.write("@%s\n%s\n+\n%s\n" % (title, seq, qual))
            else:
                seq = seq[:index]
                out_handle2.write("@%s\n%s\n+\n%s\n" % (title, seq, qual))
    out_handle2.close()




def main():
    # args = build_parser()
    # in1 = args.in1
    # in2 = args.in2
    # za = args.za
    # conf = args.conf
    # out1 = args.out1
    # out2 = args.out2

    in1 = 'A1001325-1PC-L1_n1000_1.fq'
    out1 = 'A1001325-1PC-L1_n1000_1.trim.fq'
    in2 = 'A1001325-1PC-L1_n1000_2.fq'
    out2 = 'A1001325-1PC-L1_n1000_2.trim.fq'
    za_seq = ''
    conf = "i5.v3.192.txt"
    za = 'ZA023'

    ### to pick sequence with mark of ZA from conf
    with open(conf, 'r') as r_handle:
        conf_reader = csv.DictReader(r_handle, fieldnames=['ZA', 'SEQ'], delimiter=' ')
        for row in conf_reader:
            if row['ZA'] == za:
                za_seq = row['SEQ']
            else:
                ## error log
                pass

    ## Read 1 was performed
    read_trim(in1, out1, in2, out2, za_seq)


if __name__ == '__main__':
    main()



