# -*- coding: UTF-8 -*-
import sys
import pandas as pd
import subprocess
import os


def reset_panel(panel_bedname):
    output_filename = 're_' + panel_bedname
    panel_DF = pd.read_csv(panel_bedname, sep='\t')

    panel_DF_plus = (panel_DF[panel_DF['Direction'] == '+'].copy())
    panel_DF_min = (panel_DF[panel_DF['Direction'] == '-'].copy())

    panel_DF_plus['End'] = panel_DF_plus['Start'].map(lambda x: x - 1)
    panel_DF_plus['Start'] = panel_DF_plus['Start'].map(lambda x: x - 2)

    panel_DF_min['Start'] = panel_DF_min['End']
    panel_DF_min['End'] = panel_DF_min['Start'].map(lambda x: x + 1)

    panel_DF_plus_min = pd.concat([panel_DF_plus, panel_DF_min], axis=0, ignore_index=True)
    panel_DF_reset = pd.merge(panel_DF, panel_DF_plus_min, how='left', on="No", suffixes=('', '_re'))
    panel_DF_reset['Chr_Pos'] = panel_DF_reset['Chr_re'].map(str) + "_" + panel_DF_reset['End_re'].map(str)

    panel_DF_plus_min.to_csv(output_filename, index=None, header=None, sep="\t")

    return (output_filename, panel_DF_reset)


def get_depth(bam_file_name, panel_file, panel_DF_reset):
    bam_file_name_tmp = bam_file_name.strip()
    bam_filename = os.path.basename(bam_file_name)
    sample_name = bam_filename.split(".")[0]

    recode = subprocess.call(
        "/home/pipeline/repo/samtools/bin/samtools depth " + bam_file_name_tmp + " -b " + panel_file + " > " + sample_name + ".depth",
        shell=True)

    df = pd.read_csv(sample_name + ".depth", sep="\t", names=['Chr', 'Pos', 'Depth'])
    df['Chr_Pos'] = df['Chr'].map(str) + "_" + df['Pos'].map(str)

    panel_DF_reset = pd.merge(panel_DF_reset, df, how='left', on="Chr_Pos", suffixes=('', '_res'))
    panel_DF_reset.to_csv(sample_name + ".depth.stat", index=None, sep="\t")


def remove_file(file):
    if os.path.exists(file):
        subprocess.call("rm " + file, shell=True)


def main():
    panel_bed = sys.argv[1]
    bam_list = sys.argv[2]

    (reset_panel_file, panel_DF_reset) = reset_panel(panel_bed)
    get_depth(bam_list, reset_panel_file, panel_DF_reset)
    remove_file(reset_panel_file)


if __name__ == '__main__':
    main()

