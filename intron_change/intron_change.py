#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
import sys
import csv
from operator import itemgetter
from itertools import groupby
from collections import defaultdict

def generate_intron(input_fname, output_fname):
    groups = []
    with open(input_fname) as fname:
        reader = csv.DictReader(fname, delimiter='\t')
        info = sorted(reader, key=itemgetter('name'))
        for row in info:
            exon_Starts = row['exonStarts'].split(",")
            exon_Ends = row['exonEnds'].split(",")
            if row['strand'] == '+':
                for i in range(1, int(row["exonCount"])):
                    intron = 'intronic'+str(i)
                    groups.append([row['name'], row['chrom'], row['strand'], row['name2'], exon_Ends[i-1], exon_Starts[i], intron])
            else:
                for i in range(1, int(row["exonCount"])):
                    intron = 'intronic' + str(int(row["exonCount"])-i)
                    groups.append([row['name'], row['chrom'], row['strand'], row['name2'], exon_Ends[i-1], exon_Starts[i], intron])

    with open(output_fname, 'w') as wname:
        writer = csv.writer(wname, quoting=csv.QUOTE_MINIMAL, delimiter='\t')
        header = ['TranscriptId', 'Chrom', 'Strand', 'Gene', 'IntronStart', 'IntronEnd', 'Intron']
        writer.writerow(header)
        for rows in groups:
            writer.writerow(rows)


if __name__ == '__main__':
    file = sys.argv[1]
    outfile = sys.argv[2]
    generate_intron(file, outfile)

