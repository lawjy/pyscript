#!/usr/bin/env python3
# _*_ coding:utf-8 _*
import os
import sys
import csv
import argparse
from collections import defaultdict

def build_parser():
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('config',
					type=argparse.FileType('rU'),
					default=sys.stdin,
					help='CONFIGURATION FILE')
						
	parser.add_argument('snvfile',
                        type=argparse.FileType('rU'),
                        default=sys.stdin,
                        help='SNV File')

	parser.add_argument('-o', '--outfile',
                        type=argparse.FileType('w'),
                        help='Name of the output file. EQA.csv will be tacked to the end')

	args = parser.parse_args()
	return args


def parse_filter(row, info):

	if row['GENE'] in info.keys():
		
		if row['COSMIC_AA'] in info[row['GENE']]:
			return row
			pass
		pass


def action():
	args = build_parser()
	config_info = defaultdict(list)


	for row in csv.DictReader(args.config, delimiter='\t'):
		config_info[row['gene']].append(row['pHGVS'].lstrip('p.'))

	fieldnames = ['AP7','Sample_Type','GENE','Filter','PERCENT','Total_Depth','Alter_Depth','CHR','START','REF','ALT','AAChange.refGene','ExonicFunc.refGene','COSMIC_ID','COSMIC_CDS','COSMIC_AA','COSMIC_CNT','Func.refGene','esp6500siv2_all','X1000g2015aug_all','avsnp147','Filter_order']
	writer = csv.DictWriter(args.outfile, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')   
	writer.writeheader()

	# writer.writerow()
	
	for row in csv.DictReader(args.snvfile, delimiter=','):
		return_row = parse_filter(dict(row), config_info)
		if return_row is not None :
			writer.writerow(return_row)


if __name__ == '__main__':
	action()
