import csv
import argparse
import pprint
import subprocess
import multiprocessing
import traceback
from collections import defaultdict

import pysam


def base_annovar(annPath, avinput, output):
    annovarDict = defaultdict(dict)
    annExe = annPath + '/table_annovar.pl'
    annDB = annPath + '/humandb/'
    annoCommand = annExe + ' ' + avinput + ' ' + annDB + ' -buildver hg19 -out ' + output + '/vAnno -remove -protocol refGene,esp6500siv2_all,1000g2015aug_all,avsnp147,cosmic70 -operation g,f,f,f,f -nastring NA --csvout'
    subprocess.call(annoCommand, shell=True)

    with open(output + "/" + "vAnno.hg19_multianno.csv", 'r') as fname:
        reader = csv.DictReader(fname)
        for row in reader:
            annovarDict[row['Chr'] + '_' + row['Start']] = row

    return annovarDict


def vc(sampleid, chrom, pos, refb, altb, bamFile, refGenome, length, annovarDict):
    indelSum = 0
    cvg = 0
    querySeq = defaultdict(list)

    # get reference base for interval [pos-10bp, pos+10]
    refs = pysam.FastaFile(refGenome)
    chromLength = refs.get_reference_length(chrom)
    pos0 = int(pos) - 1
    Lseq = refs.fetch(reference=chrom, start=max(0, pos0 - length), end=pos0).upper()
    refSeq = refs.fetch(reference=chrom, start=pos0 + len(refb),
                        end=min(pos0 + len(refb) + length, chromLength)).upper()
    altSeq = Lseq + altb + refSeq

    # pile up reads
    samfile = pysam.AlignmentFile(bamFile, 'rb')
    for read in samfile.pileup(region=chrom + ':' + pos + '-' + pos, truncate=True, max_depth=100000,
                               stepper='nofilter'):
        for pileupRead in read.pileups:
            # read ID
            qname = pileupRead.alignment.query_name

            # coverage -- read, not fragment
            cvg += 1

            # check if the site is insertion
            if pileupRead.indel > 0:
                frankSeq = pileupRead.alignment.query_sequence[max(0, pileupRead.query_position - length): min(
                    (pileupRead.query_position + 1 + pileupRead.indel + length), pileupRead.alignment.query_length)]
                querySeq[qname].append(frankSeq)

            elif pileupRead.indel < 0:
                frankSeq = pileupRead.alignment.query_sequence[max(0, pileupRead.query_position - length): min(
                    (pileupRead.query_position + 1 + length), pileupRead.alignment.query_length)]
                querySeq[qname].append(frankSeq)

    for info in querySeq.values():
        indelSum += int(info.count(altSeq))

    percent = "{:.2f}".format(float(indelSum) / cvg * 100)
    chrom_pos = chrom + '_' + pos

    ###output : list
    if chrom_pos in annovarDict.keys():

        outvec = [sampleid, 'NA', annovarDict[chrom_pos]['Gene.refGene'], 'REJECT', percent, cvg, indelSum,
                  chrom, pos, refb, altb, annovarDict[chrom_pos]['AAChange.refGene'],
                  annovarDict[chrom_pos]['ExonicFunc.refGene'], annovarDict[chrom_pos]['cosmic70'],
                  'NA', 'NA', 'NA', annovarDict[chrom_pos]['Func.refGene'],
                  annovarDict[chrom_pos]['esp6500siv2_all'],
                  annovarDict[chrom_pos]['1000g2015aug_all'], annovarDict[chrom_pos]['avsnp147'], 'NA',
                  'NA']
        return outvec
    else:
        outvec = []
        return outvec


def vc_wrapper(*args):
    try:
        output = vc(*args)
    except:
        print("Exception thrown in vc() function at genome location:", args[1], args[2])
        output = "Exception thrown!\n" + traceback.format_exc()
    return output


def argParseInit():
    ''' 解析命令行参数 '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--sampleid', default=None, required=True, help='Sample ID')
    parser.add_argument('--bamFile', default=None, required=True, help='BAM file')
    parser.add_argument('--bedTarget', default=None, required=True, help='BED file for target region')
    parser.add_argument('--refGenome',
                        default='/data/IndividualArchive/luojy/pipeline/ngskit2/db/Homo_sapiens_assembly19.fasta')
    parser.add_argument('--annovarPath', default=None, help='path to ANNOVAR directory')
    parser.add_argument('--out', default=None, help='path to working directory')
    args = parser.parse_args()
    return args


def main():
    # sampleid = 'example'
    # bedTarget = 'indel_imposed.bed'
    # refGenome = '/data/IndividualArchive/luojy/pipeline/ngskit2/db/Homo_sapiens_assembly19.fasta'
    # bamFile = 'proper.bam'
    # annovarPath = '/home/pipeline/helitec_pipeline/bin/annovar'
    # out = './'

    args = argParseInit()
    length = int(10)
    nCPU = int(4)
    sampleid = args.sampleid
    bedTarget = args.bedTarget
    refGenome = args.refGenome
    bamFile = args.bamFile
    annovarPath = args.annovarPath
    out = args.out

    ## annovar ;return dict
    annovarDict = base_annovar(annovarPath, bedTarget, out)

    locList = []
    for line in open(bedTarget, 'r'):
        if not line.startswith("#"):
            (chrom, pos, pos, refb, altb) = line.strip().split("\t")[0:5]
            locList.append((chrom, str(pos), refb, altb))

    pool = multiprocessing.Pool(processes=nCPU)
    results = [
        pool.apply_async(vc_wrapper, args=(sampleid, x[0], x[1], x[2], x[3], bamFile, refGenome, length, annovarDict))
        for x in locList]
    output = [p.get() for p in results]
    pool.close()
    pool.join()

    with open(out + '/' + sampleid + '.indelF.PASSnPct.txt', 'w', newline='') as wname:
        header = ['AP7', 'Sample_Type', 'GENE', 'Filter', 'PERCENT', 'Total_Depth', 'Alter_Depth', 'CHR', 'START',
                  'REF', 'ALT', 'AAChange.refGene', 'ExonicFunc.refGene', 'COSMIC_ID', 'COSMIC_CDS', 'COSMIC_AA',
                  'COSMIC_CNT', 'Func.refGen', 'esp6500siv2_all', 'X1000g2015aug_all', 'avsnp147', 'simple',
                  'Filter_order']
        writer = csv.writer(wname, delimiter='\t')
        if output:
            writer.writerow(header)
            writer.writerows(output)
        else:
            writer.writerow([])


if __name__ == '__main__':
    main()

