# -*- coding: utf-8 -*-
from email import encoders
from email.header import Header
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import smtplib
import os

def Sendmail(attach_path, massge):
	### 初始化基本信息
	send_addr = "pipeline@helitecbio.com"
	password = ""
	to_addr = ["luojy@helitecbio.com"]
	#ccto_list = ["luojy@helitecbio.com", "luojy@helitecbio.com"]
	smtp_server = 'smtp.exmail.qq.com'

	### 格式化发送名
	from_addr = u'Pipelin管理员 <%s>' % send_addr
	name, addr = parseaddr(from_addr)
	from_addr = formataddr((Header(name, 'utf-8').encode(), addr))

	### 编辑正文
	msgText = MIMEText('项目文库编号：%s\n项目芯片编号：%s\n数据存放路径：%s\n数据分析路径：%s\n结果存放路径：%s\n' % (massge[0], massge[1], massge[2], massge[3], massge[4]), 'plain', 'utf-8')
	msg = MIMEMultipart()
	msg['From'] = from_addr
	msg['To'] = ','.join(to_addr)
	msg['cc'] = ','.join(ccto_list)
	msg['Subject'] = Header(u'项目：%s 分析完毕' % massge[5], 'utf-8').encode()

	# 邮件正文是MIMEText:
	msg.attach(msgText)


	### 附件操作
	attach_file = MIMEApplication(open(attach_path,'rb').read())
	attach_file_name = os.path.basename(attach_path)
	attach_file.add_header('Content-Disposition', 'attachment', filename=attach_file_name)
	msg.attach(attach_file)


	### 执行发送操作
	receive_addr = []
	receive_addr.extend(to_addr)
	receive_addr.extend(ccto_list)

	server = smtplib.SMTP_SSL(smtp_server, 465)
	server.set_debuglevel(1)
	server.login(send_addr, password)
	server.sendmail(send_addr, receive_addr, msg.as_string())
	server.quit()




attach_path="/data/output/191008_A00159_0457_BHTLH7DSXX_LN190930IVD17_191011analysis_1.tar.gz"
massge = ["LN190930IVD17", "BHTLH7DSXX", "/data/fastq/191008_A00159_0457_BHTLH7DSXX", "/data/analysis/191008_A00159_0457_BHTLH7DSXX_LN190930IVD17_191011analysis", "/data/output/191008_A00159_0457_BHTLH7DSXX_LN190930IVD17_191011analysis", "191008_A00159_0457_BHTLH7DSXX_LN190930IVD17_191011analysis"]
Sendmail(attach_path, massge)


